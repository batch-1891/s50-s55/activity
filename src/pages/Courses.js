import {Fragment, useEffect, useState} from 'react';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';

export default function Courses() {

	// console.log(coursesData)
	// console.log(coursesData[0])

	const [courses, setCourses] = useState([])

	useEffect(() => {

		fetch('http://localhost:4000/courses/')
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setCourses(data.map(course => {

				return (
						<CourseCard key={course._id} courseProp={course} />
					)
			}))
		})

	}, [])

	// const courses = coursesData.map(course => {
	// 	return (

	// 			<CourseCard key={course.id} courseProp={course}/>

	// 		)
	// })

	return(

		
		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>


		)
}